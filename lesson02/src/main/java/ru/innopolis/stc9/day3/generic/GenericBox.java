package ru.innopolis.stc9.day3.generic;

public class GenericBox <T1> {
    private T1 value;

    public GenericBox(T1 value) {
        this.value = value;
    }

    public T1 getValue() {
        return value;
    }

    public void setValue(T1 value) {
        this.value = value;
    }
}
