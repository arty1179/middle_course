package ru.innopolis.stc9.day3.box;

public class LongBox {
    private Long value;

    public LongBox(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
