package ru.innopolis.stc9.day3.generic;

public class GenericBox2 {
    private String value;

    public GenericBox2(String value) {
        this.value = value;
    }

    public <T1> T1 method(T1 t1) {
        return t1;
    }

}
