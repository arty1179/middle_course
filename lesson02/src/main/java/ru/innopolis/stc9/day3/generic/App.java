package ru.innopolis.stc9.day3.generic;

public class App {
    public static void main(String[] args) {
        GenericBox2 gb = new GenericBox2("qwerty");
        System.out.println(gb.method(new String("mmm")).getClass());
        System.out.println(gb.method(new Integer("10")).getClass());
    }
}