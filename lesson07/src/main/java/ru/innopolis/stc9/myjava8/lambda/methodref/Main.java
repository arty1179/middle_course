package ru.innopolis.stc9.myjava8.lambda.methodref;

public class Main {
    public static void main(String[] args) {
        iGreeter sayHi = Greeter::sayHi;
        sayHi.sayHi();

        Greeter hello = new Greeter();
        iGreeter sayHi2 = hello::sayHiBrightly;
        sayHi2.sayHi();

        iGreeter hello3 = ()-> System.out.println("Salam");
        hello3.sayHi();
    }
}
