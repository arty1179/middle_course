package ru.innopolis.stc9.myjava8.interfaces;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Main {
    public static void main(String[] args) {
        //Пример консьюмера. На вход что-то дали, ничего назад не отдали
        Consumer<String> greedy = (input)-> System.out.println("I see input: "+input);
        greedy.accept("Hello");

        //Пример трех предикатов
        Predicate<Integer> predicateMoreThenTen = (num) -> {
            return num > 10;
        };
        Predicate<Integer> predicateLessThenZero = (num) -> {
            return num < 0;
        };
        Predicate<Integer> predicateIsOdd = (num) -> {
            return num % 2 == 0;
        };
        Boolean result = predicateLessThenZero
                .or(predicateMoreThenTen
                        .and(predicateIsOdd)).test(16);
        System.out.println(result);

        //Пример функции
        Function<Integer, String> intToString =
                (arg) -> arg.toString();
        Function<String, Integer> strToInt =
                (arg) -> Integer.parseInt(arg);
        System.out.println(intToString.apply(42));
        System.out.println(intToString.compose(strToInt).apply("43"));

        //Пример сапплаера, удобно строить фабрики
        Supplier<Something> somethingSupplier = Something::new;
        somethingSupplier.get();

        //Компаратор сравниваит два объекта
        Comparator<Integer> comparator = (o1, o2) -> o1 - o2;
        System.out.println(comparator.compare(5,2));

        //Optional - значение, которое может быть nullable
        Optional <String> optionalFull = Optional.of("some");
        Optional <String> optionalEmpty = Optional.ofNullable(null);
        System.out.println(optionalFull.isPresent());
        System.out.println(optionalEmpty.isPresent());
        //System.out.println(optionalEmpty.get());
        System.out.println(optionalEmpty.orElse("default"));
        optionalFull.ifPresent(
                (s) -> System.out.println(s));
    }
}
