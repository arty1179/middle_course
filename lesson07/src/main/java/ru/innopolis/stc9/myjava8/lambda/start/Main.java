package ru.innopolis.stc9.myjava8.lambda.start;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please,enter your salary:");
        Integer salary = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Please, enter your country:");
        String country = scanner.nextLine();

        TaxCalculator taxCalculator = null;
        switch (country){
            case "Russia":
                taxCalculator = (int summ)->{return salary*0.13;};
                break;
            case "USA":
                taxCalculator = (int summ)->summ*0.25;
                break;
            case "Angola":
                taxCalculator = (summ)->summ*0.90;
                break;
            default:
                taxCalculator = (int summ)->{return 0;};
                break;
        }
        System.out.println(taxCalculator.calculateTax(salary));
    }
}
