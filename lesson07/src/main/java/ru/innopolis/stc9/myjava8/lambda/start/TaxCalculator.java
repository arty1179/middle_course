package ru.innopolis.stc9.myjava8.lambda.start;

@FunctionalInterface
public interface TaxCalculator {
    double calculateTax(int summ);
}
