package com.company;

public class Main {

    /*public static void main(String[] args) {
        MyAnon anon = new MyAnon() {
            @Override
            public void saySome(String mes) {
                System.out.println(mes);
            }

            @Override
            public Integer count() {
                return 10;
            }
        };
        anon.count();
        anon.saySome("kldjfdj");
    }*/

    /*public static void main(String[] args) {
        MyAnonHolder holder = new MyAnonHolder();
        holder.setAnon(new MyAnon() {
            @Override
            public void saySome(String mes) {
                try {
                    MyAnonHolder holder1 =
                            MyAnonHolder(super.clone());
                    System.out.println(holder1.counter);
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public Integer count() {
                return 10;
            }
        });
    }*/

    public static void main(String[] args) {
        MyFunctik functik = (a) -> { return a+"1";};

        MyFunctikHolder holder = new MyFunctikHolder();
        holder.setFunctik(functik);
        holder.applyFunktik(98);
    }
}
