package com.company;

public class MyAnonHolder {
    public final int counter = 10;

    private MyAnon anon;

    public MyAnon getAnon() {
        return anon;
    }

    public void setAnon(MyAnon anon) {
        this.anon = anon;
    }
}
