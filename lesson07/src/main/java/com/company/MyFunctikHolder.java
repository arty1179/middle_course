package com.company;

public class MyFunctikHolder {
    private MyFunctik functik;

    public MyFunctik getFunctik() {
        return functik;
    }

    public void setFunctik(MyFunctik functik) {
        this.functik = functik;
    }

    public void applyFunktik(int in){
        System.out.println(functik.funk(in));
    }
}
