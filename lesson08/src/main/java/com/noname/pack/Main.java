package com.noname.pack;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 *  <class>com.noname.pack.Main</class>
 *
 * @author user
 */

public class Main {
    public static void main(String[] args) {
        // Создаем спринговый контекст
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext(new String[] {"config.xml"});
        // Получаем какойТоБин
        ConsoleHelper ch = ac.getBean(ConsoleHelper.class);
        new Main().listenConsole(ch);
    }
    public void listenConsole(ConsoleHelper ch) {
        // System.console() в идеи у меня отдает null, разбираться не стал.
        try (Reader r = new InputStreamReader(System.in); BufferedReader br = new BufferedReader(r)) {
            while (true) {
                System.out.print("Enter line: " );
                final String line = br.readLine();
                Request request = ch.execute(line);
                request.operate();
                if ("exit".equals(line)) {
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
