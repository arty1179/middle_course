package com.noname.pack;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ����� <class>com.noname.pack.ConsoleHelper</class>
 *
 * @author user
 */
public class ConsoleHelper {

    @Getter
    @Setter
    private Map<Pattern, Request> commandMap = new HashMap<>();


    public ConsoleHelper() {
    }

    public Request execute(String line) {
        for (Pattern pattern : new ArrayList<>(commandMap.keySet())) {
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                return commandMap.get(pattern);

            }
        }
        return null;
    }

}
