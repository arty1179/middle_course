package com.inno.stc.startMulti;

public class MainFast {
    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<2220;i++){
                    System.out.println(Thread.currentThread().getName()+" "+i);
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i=2220;i>0;i--){
                    System.out.println(Thread.currentThread().getName()+" "+i);
                }
            }
        });

        thread.start();
        thread2.start();
    }
}
