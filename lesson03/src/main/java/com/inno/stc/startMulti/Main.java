package com.inno.stc.startMulti;

public class Main {
    public static void main(String[] args) {
        MyThread myThread = new MyThread("Thread 1");
        myThread.start();
        MyThread myThread2 = new MyThread("Thread 2");
        myThread2.start();
        for (int i=0;i<220;i++){
            System.out.println("Thread main "+i);
        }
        try {
            myThread.join();
            myThread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Завершение основного потока");
    }
}
