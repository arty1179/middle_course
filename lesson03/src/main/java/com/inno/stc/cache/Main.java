package com.inno.stc.cache;

public class Main {
    private static volatile Integer x = 0;

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    int temp = 0;
                    for (int j = 0; j < 10; j++) {
                        temp++;
                        //x++;
                    }
                    synchronized (x) {
                        x += temp;
                    }
                    System.out.println(Thread.currentThread().getName() + " x = " + x);
                }
            });
            thread.start();
        }
    }
}
