package com.inno.stc.MySync;

public class MySync {

    public static void main(String[] args) {
        Object monitor = new Object();
        for (int i=0;i<100;i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName() + " started");
                    synchronized (monitor) {
                        System.out.println(Thread.currentThread().getName() + " locked monitor");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        System.out.println(Thread.currentThread().getName() + " releasing monitor");
                    }
                }
            });
            thread.start();
        }
    }
}
