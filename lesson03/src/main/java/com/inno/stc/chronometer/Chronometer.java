package com.inno.stc.chronometer;

import java.util.ArrayList;

public class Chronometer extends Thread {
    private TimeKeeper timeKeeper;
    private int iterations;
    private int step;
    private ArrayList<SlaveCounter> childThreads;

    public Chronometer(TimeKeeper timeKeeper, int iterations, int step, ArrayList<SlaveCounter> childThreads) {
        this.timeKeeper = timeKeeper;
        this.iterations = iterations;
        this.childThreads = childThreads;
        this.step = step;
    }

    @Override
    public void run() {
        for (int i=0; i<iterations; i++){
            try {
                Thread.sleep(step);
                timeKeeper.incTime();
                System.out.println(timeKeeper.getTime());
                synchronized (timeKeeper) {
                    timeKeeper.notifyAll();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        stopEveryThread();
    }

    private void stopEveryThread(){
        childThreads.forEach((childThread)->childThread.timeToStopOn());
        synchronized (timeKeeper) {
            timeKeeper.notifyAll();
        }
    }
}
