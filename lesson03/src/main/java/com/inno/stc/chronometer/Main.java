package com.inno.stc.chronometer;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        TimeKeeper timeKeeper = new TimeKeeper();
        ArrayList<SlaveCounter> slaveThreads = new ArrayList();
        slaveThreads.add(new SlaveCounter(timeKeeper, 5));
        slaveThreads.add(new SlaveCounter(timeKeeper, 7));
        Chronometer chronometer = new Chronometer(timeKeeper, 200, 1000, slaveThreads);
        chronometer.start();
/*        for (Thread slaveThread : slaveThreads) {
            slaveThread.start();
        }*/
        slaveThreads.forEach((slaveThread)->slaveThread.start());
    }
}
