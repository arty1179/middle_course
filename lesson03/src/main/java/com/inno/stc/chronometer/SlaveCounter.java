package com.inno.stc.chronometer;

public class SlaveCounter extends Thread{
    TimeKeeper timeKeeper;
    private int interval;
    private boolean timeToStop = false;

    public SlaveCounter(TimeKeeper timeKeeper, int interval) {
        this.timeKeeper = timeKeeper;
        this.interval = interval;
    }

    public void timeToStopOn(){
        timeToStop = true;
    }

    @Override
    public void run() {
        do {
            synchronized (timeKeeper) {
                try {
                    timeKeeper.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if ((timeKeeper.getTime()!=0)&&(timeKeeper.getTime() % interval)==0){
                System.out.println("Thread " + interval + " reacted");
            }
        }
        while (!timeToStop);
    }
}
