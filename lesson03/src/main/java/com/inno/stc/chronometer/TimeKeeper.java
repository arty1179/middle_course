package com.inno.stc.chronometer;

public class TimeKeeper {
    private int time;

    public synchronized int getTime() {
        return time;
    }

    public synchronized void incTime() {
        time++;
    }
}
