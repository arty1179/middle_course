package exceptions;

public class Main {
    public static void main(String[] args) {
        System.out.println(calculate());
    }

    private static int calculate() {
        double res = 0;
        try {
            res = divider(6, 7);
            return 13;
        }
/*        catch (MyArithmeticException e) {
            res = specialCaseFor3(6, 3);
            return 12;
        }*/
        finally {
            System.out.println(res);
            return 15;
        }
    }

    static double divider(int a, int b) throws MyArithmeticException {
        if (b % 3 == 0) {
            throw new MyArithmeticException("3");
        } else {
            if (b != 0) {
                return a / b;
            } else {
                return 0;
            }
        }
    }

    private static double specialCaseFor3(int a, int b) {
        System.out.println("Some special logics");
        return a / b / 2;
    }
}
